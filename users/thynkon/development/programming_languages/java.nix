{ config, pkgs, ... }:

{
	# Define a user account. Don't forget to set a password with ‘passwd’.
	users.users.thynkon = {
		packages = with pkgs; [
			android-studio
			jdk
			maven
		];
	};

        programs.java.enable = true;
}
